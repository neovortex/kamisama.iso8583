﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Client.Exceptions;

/// <summary>
/// SendingMessageFailedException thrown when sending message into the stream fails
/// </summary>
[ExcludeFromCodeCoverage]
public class SendingMessageFailedException : Exception
{
	/// <summary>
	/// constructor
	/// </summary>
	public SendingMessageFailedException()
	{
	}
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="message"></param>
	public SendingMessageFailedException(string message) : base(message)
	{
	}
	/// <summary>
	/// constructor
	/// </summary>
	/// <param name="message"></param>
	/// <param name="innerException"></param>
	public SendingMessageFailedException(string message, Exception innerException) : base(message, innerException)
	{
	}
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace KamiSama.Iso8583
{
	/// <summary>
	/// Iso Parts Collection
	/// </summary>
	[ExcludeFromCodeCoverage]
	public class IsoPartsCollection : Dictionary<string, IsoPart>, IDictionary<string, object>
	{
		/// <summary>
		/// used to avoid excessive casting
		/// </summary>
		private readonly IDictionary<string, IsoPart> _this;
		/// <inheritdoc />
		object IDictionary<string, object>.this[string key]
		{
			get => this[key];
			set => this[key] = (IsoPart)value;
		}
		/// <inheritdoc />
		ICollection<string> IDictionary<string, object>.Keys => _this.Keys;
		/// <inheritdoc />
		ICollection<object> IDictionary<string, object>.Values => _this.Values.Cast<object>().ToArray();
		/// <inheritdoc />
		int ICollection<KeyValuePair<string, object>>.Count => _this.Count;
		/// <inheritdoc />
		bool ICollection<KeyValuePair<string, object>>.IsReadOnly => throw new NotImplementedException();
		/// <summary>
		/// constructor
		/// </summary>
		public IsoPartsCollection()
			: base(StringComparer.InvariantCultureIgnoreCase)
		{
			_this = this;
		}
		private static KeyValuePair<string, object> ToObjectKeyPair(KeyValuePair<string, IsoPart> item)
			=> KeyValuePair.Create<string, object>(item.Key, item.Value);
		private static KeyValuePair<string, IsoPart> ToPartKeyPair(KeyValuePair<string, object> item)
			=> KeyValuePair.Create(item.Key, (IsoPart)item.Value);
		/// <inheritdoc />
		void IDictionary<string, object>.Add(string key, object value) => _this.Add(key, (IsoPart)value);
		/// <inheritdoc />
		bool IDictionary<string, object>.ContainsKey(string key) => _this.ContainsKey(key);
		/// <inheritdoc />
		bool IDictionary<string, object>.Remove(string key) => _this.Remove(key);
		/// <inheritdoc />
		bool IDictionary<string, object>.TryGetValue(string key, out object value)
		{
			if (_this.TryGetValue(key, out IsoPart part))
			{
				value = part;
				return true;
			}
			value = null;
			return false;
		}
		/// <inheritdoc />
		void ICollection<KeyValuePair<string, object>>.Add(KeyValuePair<string, object> item)
			=> _this.Add(ToPartKeyPair(item));
		/// <inheritdoc />
		void ICollection<KeyValuePair<string, object>>.Clear() => _this.Clear();
		/// <inheritdoc />
		bool ICollection<KeyValuePair<string, object>>.Contains(KeyValuePair<string, object> item) => _this.Contains(ToPartKeyPair(item));
		/// <inheritdoc />
		void ICollection<KeyValuePair<string, object>>.CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
		{
			foreach (var item in _this)
				array[arrayIndex++] = ToObjectKeyPair(item);
		}
		/// <inheritdoc />
		bool ICollection<KeyValuePair<string, object>>.Remove(KeyValuePair<string, object> item) => _this.Remove(ToPartKeyPair(item));
		/// <inheritdoc />
		IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator()
		{
			foreach (var item in _this)
				yield return ToObjectKeyPair(item);
		}
	}
}
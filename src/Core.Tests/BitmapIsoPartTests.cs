﻿using KamiSama.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace KamiSama.Iso8583.Tests
{
	[TestClass]
	[ExcludeFromCodeCoverage]
	public class BitmapIsoPartTests
	{
		[TestMethod]
		public void Indexer_must_set_correct_value_for_numbers_between_1_and_128()
		{
			var part = new BitmapIsoPart();

			part[1] = true;
			part[64] = true;
			part[66] = true;

			Assert.IsTrue(part[1]);
			Assert.IsTrue(part[64]);
			Assert.IsTrue(part[66]);
			Assert.IsTrue(part[2]); //equals to 66
			Assert.IsTrue(part[128]);
		}
		[TestMethod]
		public void Indexer_throws_IndexOutOfRangeException_for_out_of_range_indexes()
		{
			var part = new BitmapIsoPart();

			//setter method
			Assert.ThrowsException<IndexOutOfRangeException>(() => part[0] = true);
			Assert.ThrowsException<IndexOutOfRangeException>(() => part[129] = true);
			//getter method
			Assert.ThrowsException<IndexOutOfRangeException>(() => part[0]);
			Assert.ThrowsException<IndexOutOfRangeException>(() => part[129]);
		}
		[TestMethod]
		public void FromHex_must_create_BitmapIsoPart_from_hex_value()
		{
			var part = BitmapIsoPart.FromHex("F23C840188E10000");
			var fields = new int[] { 1, 2, 3, 4, 7, 11, 12, 13, 14, 17, 22, 32, 33, 37, 41, 42, 43, 48 };

			for (int i = 1; i < 64; i++)
				Assert.AreEqual(fields.Contains(i), part[i]);
		}
		[TestMethod]
		public void ToBytes_must_create_BitmapIsoPart_from_binary_value()
		{
			var part = BitmapIsoPart.FromBytes("F23C840188E10000".FromHex());
			var fields = new int[] { 1, 2, 3, 4, 7, 11, 12, 13, 14, 17, 22, 32, 33, 37, 41, 42, 43, 48 };

			for (int i = 1; i < 64; i++)
				Assert.AreEqual(fields.Contains(i), part[i]);
		}
		[TestMethod]
		public void ToBytes_must_return_corresponding_byte_array_to_bitmap()
		{
			var data = "F23C840188E10000".FromHex();
			var part = BitmapIsoPart.FromBytes(data);

			var result = part.ToBytes();

			CollectionAssert.AreEqual(data, result.ToArray());
		}

		[TestMethod]
		public void ToDebugString_must_display_primary_bitmap_fields_for_field_no_0()
		{
			var primaryBitmap = BitmapIsoPart.FromHex("F23C840188E10000");
			Assert.AreEqual("(Bitmap) hex: F23C840188E10000 fields: P01, P02, P03, P04, P07, P11, P12, P13, P14, P17, P22, P32, P33, P37, P41, P42, P43, P48", primaryBitmap.ToDebugString(0));
		}
		[TestMethod]
		public void ToDebugString_must_display_secondary_bitmap_fields_for_field_no_1()
		{
			var secondaryBitmap = BitmapIsoPart.FromHex("0100000010000000");
			Assert.AreEqual("(Bitmap) hex: 0100000010000000 fields: S72, S100", secondaryBitmap.ToDebugString(1));
		}
		[TestMethod]
		public void ToString_displays_hex_value()
		{
			var field = BitmapIsoPart.FromHex("F23C840188E10000");

			Assert.AreEqual("F23C840188E10000", field.ToString());
		}
		[TestMethod]
		public void ToString_displays_zero_hex_for_no_flag()
		{
			var field = new BitmapIsoPart();

			Assert.AreEqual("0000000000000000", field.ToString());
		}
	}
}
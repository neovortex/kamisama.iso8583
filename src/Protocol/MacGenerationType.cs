﻿namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// Message Authentication Code Type
	/// </summary>
	public enum MacGenerationType : int
	{
		/// <summary>
		/// jsut skips generation and reuse current value
		/// </summary>
		Skip = 0,
		/// <summary>
		/// ANSI X9.19 MAC algorithm
		/// </summary>
		AnsiX9_19 = 1,
		/// <summary>
		/// DES MAC algorithm
		/// </summary>
		DesMac = 2,
		/// <summary>
		/// Triple DES (3DES) MAC algorithm
		/// </summary>
		TripleDesMac = 3,
		/// <summary>
		/// AES MAC algorithm
		/// </summary>
		AesMac = 4
	}
}

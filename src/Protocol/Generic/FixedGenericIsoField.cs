﻿using KamiSama.Iso8583.Protocol.Parts;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Generic
{
	/// <summary>
	/// This class represents fixed length iso field in iso message
	/// </summary>
	[Serializable]
	public class FixedGenericIsoField : GenericIsoProtocolField, IEquatable<FixedGenericIsoField>
	{
		/// <summary>
		/// the length of the field
		/// </summary>
		[XmlAttribute("length")]
		public int Length { get; set; }
		/// <inheritdoc />
		public override IsoFieldDefinition ToIsoFieldDefinition()
		{
			if ((this.FieldNo == 64) || (this.FieldNo == 128))
			{
				if ((this.DataType == "hex-binary") || (DataType == "binary-hex"))
					return new IsoFieldDefinition
					{
						FieldNo = this.FieldNo,
						Type = $"fixed({Length}) hex-mac"
					};
				else
					return new IsoFieldDefinition
					{
						FieldNo = this.FieldNo,
						Type = $"fixed({Length}) mac"
					};
			}

			string dataType = DataType switch
			{
				"text" or "txt" => "text " + Encoding,
				_ => DataType
			};

			return new IsoFieldDefinition
			{
				FieldNo = this.FieldNo,
				Type = $"fixed({Length}) {dataType}"
			};
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(FixedGenericIsoField other)
			=> (other != null) &&
				(this.DataType == other.DataType) &&
				(this.Encoding == other.Encoding) &&
				(this.FieldNo == other.FieldNo) &&
				(this.Length == other.Length);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as FixedGenericIsoField);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}
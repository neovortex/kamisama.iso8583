﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Thrown when something is wrong in parsing the protocol file
	/// </summary>
	/// <remarks>
	/// constructor
	/// </remarks>
	/// <param name="message"></param>
	/// <param name="innerException"></param>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class IsoProtocolParsingException(string message, Exception innerException = null) : Exception(message, innerException)
	{
	}
}

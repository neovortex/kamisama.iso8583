﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// Cannot Pick message bytes from stream, see the message
	/// </summary>
	/// <remarks>
	/// constructor
	/// </remarks>
	/// <param name="message"></param>
	/// <param name="innerException"></param>
	[ExcludeFromCodeCoverage]
	[Serializable]
	public class CannotPickMessageBytesFromStreamException(string message = "Cannot pick message bytes from stream.", Exception innerException = null) : IsoMessageReadException(message, innerException)
	{
	}
}

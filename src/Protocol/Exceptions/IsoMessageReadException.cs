﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Exceptions
{
	/// <summary>
	/// IsoMessageReadException; this exception will be thrown in different iso message parsing stages
	/// </summary>
	/// <remarks>
	/// constructor
	/// </remarks>
	/// <param name="message"></param>
	/// <param name="innerException"></param>
	[Serializable]
	[ExcludeFromCodeCoverage]
    public class IsoMessageReadException(string message, Exception innerException = null) : Exception(message, innerException)
	{
	}
}

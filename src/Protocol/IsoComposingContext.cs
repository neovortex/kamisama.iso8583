﻿using System;
using System.IO;

namespace KamiSama.Iso8583.Protocol
{
	/// <summary>
	/// the context used for message parsing
	/// </summary>
	public class IsoComposingContext : IIsoProtocolContext
	{
		/// <summary>
		/// inner stream of composing context
		/// </summary>
		internal readonly MemoryStream stream;
		/// <summary>
		/// Bytes used for Iso Message MAC
		/// </summary>
		public ReadOnlyMemory<byte> ForMacBytes { get; set; }
		/// <summary>
		/// Total Message Body
		/// </summary>
		public ReadOnlyMemory<byte> MessageBodyBytes => stream.ToArray();
		/// <inheritdoc />
		public IsoMessage Message { get; set; }
		/// <summary>
		/// Parent assigned context
		/// </summary>
		public IsoComposingContext ParentContext { get; }
		/// <summary>
		/// constrcutor
		/// </summary>
		/// <param name="message">iso message</param>
		public IsoComposingContext(IsoMessage message)
		{
			this.stream = new MemoryStream();
			this.Message = message;
		}
		/// <summary>
		/// creates a context from parent context
		/// </summary>
		/// <param name="parentContext"></param>
		public IsoComposingContext(IsoComposingContext parentContext)
		{
			this.Message = parentContext.Message;
			this.ParentContext = parentContext;
			this.stream = new MemoryStream();
		}
		/// <summary>
		/// Adds some byte to the message
		/// </summary>
		/// <param name="data"></param>
		public void Write(ReadOnlyMemory<byte> data) => stream.Write(data.Span);
		/// <summary>
		/// Creates a sub context for inner process
		/// </summary>
		/// <returns></returns>
		public IsoComposingContext CreateSubContext() => new(this);
		/// <summary>
		/// returns total bytes written in the context
		/// </summary>
		/// <returns></returns>
		public byte[] ToArray() => stream.ToArray();
	}
}

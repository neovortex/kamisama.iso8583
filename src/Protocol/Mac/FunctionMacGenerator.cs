﻿using System;
using System.Threading.Tasks;

namespace KamiSama.Iso8583.Protocol.Mac
{
	/// <summary>
	/// Function based Mac Generator
	/// </summary>
	/// <remarks>
	/// constructor
	/// </remarks>
	/// <param name="func"></param>
	public class FunctionMacGenerator(Func<IIsoProtocolContext, MacGenerationType, Task<byte[]>> func) : IMacGenerator
	{
		/// <summary>
		/// function to generate mac
		/// </summary>
		public Func<IIsoProtocolContext, MacGenerationType, Task<byte[]>> Func { get; } = func;

		/// <inheritdoc />
		public Task<byte[]> GenerateMacAsync(IIsoProtocolContext context, MacGenerationType macGenerationType) => Func(context, macGenerationType);
	}
}

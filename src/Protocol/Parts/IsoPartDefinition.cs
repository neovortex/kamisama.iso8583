﻿using Expressive;
using System;
using System.Diagnostics.CodeAnalysis;
using System.Xml.Serialization;

namespace KamiSama.Iso8583.Protocol.Parts
{
	/// <summary>
	/// Iso Part Definitaion abstract class
	/// </summary>
	public class IsoPartDefinition : IsoPartDefinitionBase, IEquatable<IsoPartDefinition>
	{
		/// <summary>
		/// Condition by which iso part should be parsed or composed
		/// </summary>
		[XmlAttribute("condition")]
		public string Condition { get; set; }
		internal Expression conditionExpression;
		/// <summary>
		/// Parses a iso part based on the definition if <see cref="Condition"/> matches
		/// </summary>
		/// <param name="context"></param>
		public new virtual void Parse(IsoParsingContext context)
		{
			if (conditionExpression.Evaluate<bool>(context.Message.Parts))
				base.Parse(context);
		}
		/// <summary>
		/// Composes a iso part based on the definition if <see cref="Condition"/> matches
		/// </summary>
		/// <param name="context"></param>
		public virtual void Compose(IsoComposingContext context)
		{
			if (conditionExpression.Evaluate<bool>(context.Message.Parts))
				base.Compose(context, null);
		}
		/// <summary>
		/// Optmizes the iso part definition
		/// </summary>
		/// <param name="forceOptimize"></param>
		public override void Optimize(bool forceOptimize = false)
		{
			if ((conditionExpression != null) && !forceOptimize)
				return;

			if (string.IsNullOrWhiteSpace(Condition))
				conditionExpression = new Expression("true", ExpressiveOptions.IgnoreCaseAll);
			else
				conditionExpression = new Expression(Condition, ExpressiveOptions.IgnoreCaseAll);

			base.Optimize(forceOptimize);
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public bool Equals(IsoPartDefinition other)
		{
			if ((other == null) ||
				(Condition != other.Condition) ||
				(Type != other.Type) ||
				(MappedName != other.MappedName) ||
				(InnerParts?.Length != other.InnerParts?.Length))
				return false;

			for (int i = 0; i < (InnerParts?.Length ?? 0); i++)
				if (!InnerParts[i].Equals(other.InnerParts[i]))
					return false;

			return true;
		}
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override bool Equals(object obj) => Equals(obj as IsoPartDefinition);
		/// <inheritdoc />
		[ExcludeFromCodeCoverage]
		public override int GetHashCode() => base.GetHashCode();
	}
}
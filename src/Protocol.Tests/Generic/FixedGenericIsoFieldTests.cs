﻿using KamiSama.Iso8583.Protocol.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KamiSama.Iso8583.Protocol.Tests.Generic
{
	[TestClass]
	public class FixedGenericIsoFieldTests
	{
		[TestMethod]
		[DataRow(64, "fixed(16) mac")]
		[DataRow(128, "fixed(16) mac")]
		public void ToIsoFieldDefinition_creates_mac_field_for_p64_and_s128(int fieldNo, string expectedType)
		{
			var field = new FixedGenericIsoField
			{
				FieldNo = fieldNo,
				Length = 16
			};

			var result = field.ToIsoFieldDefinition();

			Assert.AreEqual(fieldNo, result.FieldNo);
			Assert.AreEqual(expectedType, result.Type);
		}
		[TestMethod]
		[DataRow("text", "utf-8", "fixed(6) text utf-8")]
		[DataRow("txt", "utf-8", "fixed(6) text utf-8")]
		public void ToIsoFieldDefinition_creates_text_field_includes_encoding(string type, string encoding, string expectedType)
		{
			var field = new FixedGenericIsoField
			{
				FieldNo = 3,
				Length = 6,
				DataType = type,
				Encoding = encoding
			};

			var result = field.ToIsoFieldDefinition();

			Assert.AreEqual(field.FieldNo, result.FieldNo);
			Assert.AreEqual(expectedType, result.Type);
		}
		[TestMethod]
		[DataRow("binary", null, "fixed(6) binary")]
		[DataRow("binary", "utf-8", "fixed(6) binary")]
		[DataRow("hex-binary", "utf-8", "fixed(6) hex-binary")]
		[DataRow("hex-binary", null, "fixed(6) hex-binary")]
		public void ToIsoFieldDefinition_creates_other_fields_without_encoding(string type, string encoding, string expectedType)
		{
			var field = new FixedGenericIsoField
			{
				FieldNo = 3,
				Length = 6,
				DataType = type,
				Encoding = encoding
			};

			var result = field.ToIsoFieldDefinition();

			Assert.AreEqual(field.FieldNo, result.FieldNo);
			Assert.AreEqual(expectedType, result.Type);
		}
	}
}

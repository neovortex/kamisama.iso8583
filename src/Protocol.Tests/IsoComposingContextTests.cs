﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KamiSama.Iso8583.Protocol.Tests
{
	[TestClass]
	public class IsoComposingContextTests
	{
		[TestMethod]
		public void Constructor_by_message_sets_message_and_stream()
		{
			var message = new IsoMessage(1200);
			var context = new IsoComposingContext(message);

			Assert.AreSame(message, context.Message);
			Assert.IsNull(context.ParentContext);
			Assert.IsNotNull(context.stream);
		}
		[TestMethod]
		public void Constrcutor_from_parent_context_copies_items_from_parent_context_except_stream()
		{
			var message = new IsoMessage(1200);
			var parentContext = new IsoComposingContext(message);
			var context = new IsoComposingContext(parentContext);

			Assert.AreSame(message, context.Message);
			Assert.AreSame(parentContext, context.ParentContext);
			Assert.IsNotNull(context.stream);
			Assert.AreNotEqual(parentContext.stream, context.stream);
		}
	}
}

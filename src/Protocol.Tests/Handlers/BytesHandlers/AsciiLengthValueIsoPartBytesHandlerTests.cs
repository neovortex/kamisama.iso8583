﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers
{
	[TestClass]
	public class AsciiLengthValueIsoPartBytesHandlerTests
	{
		[TestMethod]
		[DataRow("lvar", true)]
		[DataRow("llvar", true)]
		[DataRow("lllvar", true)]
		[DataRow("llllvar", true)]
		[DataRow("lllllvar", true)]
		[DataRow("ascii-lvar", true)]
		[DataRow("ascii-llvar", true)]
		[DataRow("ascii-lllvar", true)]
		[DataRow("ascii-llllvar", true)]
		[DataRow("ascii-lllllvar", true)]
		[DataRow("invalid-type", false)]
		[DataRow("l", false)]
		[DataRow("var", false)]
		public void Matches_matches_type_string(string type, bool expectedResult)
			=> Assert.AreEqual(expectedResult, AsciiLengthValueIsoPartBytesHandler.Matches(type));
		[TestMethod]
		[DataRow("lvar", 1)]
		[DataRow("llvar", 2)]
		[DataRow("lllvar", 3)]
		[DataRow("llllvar", 4)]
		[DataRow("lllllvar", 5)]
		[DataRow("ascii-lvar", 1)]
		[DataRow("ascii-llvar", 2)]
		[DataRow("ascii-lllvar", 3)]
		[DataRow("ascii-llllvar", 4)]
		[DataRow("ascii-lllllvar", 5)]
		public void Create_creates_AsciiLengthValueIsoPartBytesHandler_with_correct_length(string type, int expectedLengthLength)
		{
			var handler = AsciiLengthValueIsoPartBytesHandler.Create(type);

			Assert.IsNotNull(handler);
			Assert.AreEqual(expectedLengthLength, handler.LengthLength);
		}
		[TestMethod]
		[DataRow("invalid-type")]
		[DataRow("l")]
		[DataRow("var")]
		[DataRow(null)]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Create_throws_IsoProtocolParsingException_if_type_is_invalid(string type)
			=> AsciiLengthValueIsoPartBytesHandler.Create(type);

		[TestMethod]
		[DataRow("lvar", "320102", "0102")]
		[DataRow("llvar", "30320102", "0102")]
		[DataRow("lllvar", "3030320102", "0102")]
		[DataRow("llllvar", "303030320102", "0102")]
		[DataRow("lllllvar", "30303030320102", "0102")]
		public void ReadBytes_reads_length_then_data_from_context_correctly(string type, string messageBytesHex, string expectedDataHex)
		{
			var handler = AsciiLengthValueIsoPartBytesHandler.Create(type);

			var context = new IsoParsingContext(messageBytesHex.FromHex());

			var data = handler.ReadBytes(context);

			Assert.AreEqual(expectedDataHex, data.ToArray().ToHex());
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageReadException))]
		public void ReadBytes_throws_IsoMessageReadException_when_length_is_invalid()
		{
			var messageBytesHex = "430102";
			var handler = AsciiLengthValueIsoPartBytesHandler.Create("lvar");

			var context = new IsoParsingContext(messageBytesHex.FromHex());

			handler.ReadBytes(context);
		}
		[TestMethod]
		[DataRow("lvar", "0102", "320102")]
		[DataRow("llvar", "0102", "30320102")]
		[DataRow("lllvar", "0102", "3030320102")]
		[DataRow("llllvar", "0102", "303030320102")]
		[DataRow("lllllvar", "0102", "30303030320102")]
		public void WriteBytes_writes_length_then_data_to_the_context_correctly(string type, string dataHex, string expectedMessageBytesHex)
		{
			var handler = AsciiLengthValueIsoPartBytesHandler.Create(type);

			var context = new IsoComposingContext(new IsoMessage(0200));

			handler.WrtieBytes(context, dataHex.FromHex());

			Assert.AreEqual(expectedMessageBytesHex, context.ToArray().ToHex());
		}
		[TestMethod]
		[ExpectedException(typeof(IsoMessageWriteException))]
		public void WriteBytes_throws_IsoMessageWriteException_when_data_exceeds_maximum_length_acceptable()
		{
			var dataHex = "0102030405060708090A";
			var handler = AsciiLengthValueIsoPartBytesHandler.Create("lvar"); //maximum length is 9

			var context = new IsoComposingContext(new IsoMessage(0200));

			handler.WrtieBytes(context, dataHex.FromHex());
		}
	}
}

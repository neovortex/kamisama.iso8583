﻿using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using System;
using System.Diagnostics.CodeAnalysis;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers
{
	[TestClass]
	public class DefaultIsoPartBytesHandlerFactoryTests
	{
		public abstract class SampleIsoPartBytesHandlerWithStaticMethods : IIsoPartBytesHandler
		{
			public abstract ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context);
			public abstract void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data);

			public static bool Matches(string type) => type == "example-type";
			[SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>")]
			public static SampleIsoPartBytesHandlerWithStaticMethods Create(string type) => Substitute.For<SampleIsoPartBytesHandlerWithStaticMethods>();
		}
		public abstract class SampleIsoPartBytesHandlerWithoutStaticMethods : IIsoPartBytesHandler
		{
			public abstract ReadOnlyMemory<byte> ReadBytes(IsoParsingContext context);
			public abstract void WrtieBytes(IsoComposingContext context, ReadOnlyMemory<byte> data);
		}
		[TestMethod]
		[DataRow("example-type", true)]
		[DataRow("another-type", false)]
		public void Matches_calls_static_match_method(string type, bool expectedResult)
			=> Assert.AreEqual(expectedResult, new DefaultIsoPartBytesHandlerFactory<SampleIsoPartBytesHandlerWithStaticMethods>().Matches(type));
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Matches_throw_IsoProtocolParsingException_if_no_static_match_method_is_found()
			=> new DefaultIsoPartBytesHandlerFactory<SampleIsoPartBytesHandlerWithoutStaticMethods>().Matches("example-type");
		[TestMethod]
		public void Create_calls_static_create_method()
			=> Assert.IsInstanceOfType(new DefaultIsoPartBytesHandlerFactory<SampleIsoPartBytesHandlerWithStaticMethods>().Create("example-type"), typeof(SampleIsoPartBytesHandlerWithStaticMethods));
		[TestMethod]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Create_throw_IsoProtocolParsingException_if_no_static_match_method_is_found()
			=> new DefaultIsoPartBytesHandlerFactory<SampleIsoPartBytesHandlerWithoutStaticMethods>().Create("example-type");
	}
}

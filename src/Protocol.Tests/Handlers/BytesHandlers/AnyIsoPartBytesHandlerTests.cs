﻿using KamiSama.Extensions;
using KamiSama.Iso8583.Protocol.Exceptions;
using KamiSama.Iso8583.Protocol.Handlers.BytesHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace KamiSama.Iso8583.Protocol.Tests.Handlers.BytesHandlers
{
	[TestClass]
	public class AnyIsoPartBytesHandlerTests
	{
		[TestMethod]
		[DataRow("any", true)]
		[DataRow("all", true)]
		[DataRow("*", true)]
		[DataRow("anyy", false)]
		[DataRow("alll", false)]
		[DataRow(null, false)]
		[DataRow("", false)]
		public void Matches_matches_type_string(string type, bool expectedResult)
			=> Assert.AreEqual(expectedResult, AnyIsoPartBytesHandler.Matches(type));
		[TestMethod]
		[DataRow("any")]
		[DataRow("all")]
		[DataRow("*")]
		public void Create_creates_AnyIsoPartTypeHandler(string type)
		{
			var handler = AnyIsoPartBytesHandler.Create(type);

			Assert.IsNotNull(handler);
		}
		[TestMethod]
		[DataRow("anyy")]
		[DataRow("alll")]
		[DataRow(null)]
		[DataRow("")]
		[ExpectedException(typeof(IsoProtocolParsingException))]
		public void Create_throws_IsoProtocolParsingException_if_type_is_invalid(string type)
			=> AnyIsoPartBytesHandler.Create(type);

		[TestMethod]
		public void ReadBytes_reads_to_the_end_of_the_context()
		{
			var data = "010203040506".FromHex();
			var handler = AnyIsoPartBytesHandler.Create("any");

			var context = new IsoParsingContext(data);

			var result = handler.ReadBytes(context);

			CollectionAssert.AreEqual(data, result.ToArray());
			Assert.IsTrue(context.CurrentPosition >= context.ContextBytes.Length);
		}
		[TestMethod]
		public void WriteBytes_writes_raw_data_into_the_context()
		{
			var data = "0102030405".FromHex();
			var handler = AnyIsoPartBytesHandler.Create("any");

			var context = new IsoComposingContext(new IsoMessage(0200));

			handler.WrtieBytes(context, data);

			CollectionAssert.AreEqual(data, context.ToArray());
		}
	}
}
